## Standalone setup
To change the default redis server :
Set env variable : redisurl

To start :
`npm start`

Then go to yourip:5000

## Docker setup :

```
docker network create georedis
docker run -d --network georedis -p 6379:6379 --name redis redis:alpine
docker run -d --network georedis -p 5000:5000 -e redisurl=redis --name georedis registry.gitlab.com/cosme/georedis-node:latest
```

Then go to yourip:5000