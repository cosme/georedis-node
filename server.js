// require the dependencies we installed
var express = require('express');
var cors = require('cors')

var app = express();
app.use(cors());

var responseTime = require('response-time');
var redis = require('redis');
var http = require('http');
var rk = require('rk'); // Concats strings together with a ":"

// create a new redis client and connect to our local redis instance
var client = redis.createClient(6379, process.env.redisurl || 'localhost');
console.log("Connecting to : " + (process.env.redisurl || 'localhost'));
// if an error occurs, print it to the console
client.on('error', function(err) {
    console.log("Error " + err);
});

app.set('port', (process.env.PORT || 5000));
// set up the response-time middleware
app.use(responseTime());

app.use(responseTime((req, res, time) => {
    console.log('{"url": "' + req.url + '", "duration": ' + time + '}');
}));

app.use(express.static('public'));




var key = "taxis";

app.get('/geo/:x/:y/:dist', function(req, res) {
    var x = req.params.x;
    var y = req.params.y;
    var dist = req.params.dist;
    var max = req.query.max || 10000;
    client.georadius(rk(key, 'geo'), x, y, dist, 'm', 'WITHCOORD', 'WITHDIST', 'ASC', 'COUNT', max, function(error, result) {
        if (error) {
            res.send('Error : ' + error)
        } else {
            // Simuler le mouvement des taxis
            // result.forEach(function(element) {
            //     client.geoadd(rk(key, 'geo'), parseFloat(element[2][0]) + (Math.random() * 2 - 1) / 10000, parseFloat(element[2][1]) + (Math.random() * 2 - 1) / 5000, element[0])
            // });
            //result.forEach(function(el) {
            //    ret.push({ "codeTaxi": el[0], "timestamp": now(), "latitude": el[1][0], "longitude": el[1][1] });
            //});
            res.send(result);

        }
    });
});

app.get('/geoall/:x/:y', function(req, res) {
    var x = req.params.x;
    var y = req.params.y;
    var dist = 100000;
    client.georadius(rk(key, 'geo'), x, y, dist, 'm', 'WITHCOORD', function(error, result) {
        if (error) {
            res.send('Error : ' + error)
        } else {
            // Simuler le mouvement des taxis
            // result.forEach(function(element) {
            //     client.geoadd(rk(key, 'geo'), parseFloat(element[1][0]) + (Math.random() * 2 - 1) / 10000, parseFloat(element[1][1]) + (Math.random() * 2 - 1) / 5000, element[0])
            // });
            ret = [];
            result.forEach(function(el) {
                ret.push({
                    "codeTaxi": el[0],
                    "timestamp": Date.now(),
                    "latitude": parseFloat(el[1][0]),
                    "longitude": parseFloat(el[1][1])
                });
            });
            res.send(ret);
        }
    });
});

app.get('/geoall', function(req, res) {
    var options = {
        host: '172.27.20.106',
        port: 8080,
        path: '/dws/getAllTaxis',
        timeout: 10000,
        headers: {
            //accept: 'application/json',
            authorization: 'Basic Y29yYWxpZTpjb3JhbGll'
        }
    };
    var req = http.request(options, function(resp) {
        let data = '';

        // A chunk of data has been recieved.
        resp.on('data', (chunk) => {
            data += chunk;
        });

        // The whole response has been received. Print out the result.
        resp.on('end', () => {
            // save for future cache
            data.forEach(function(element) {
                client.set(rk(key, 'taxis', data.codeTaxi), data)
            });
            res.send(JSON.parse(data));
        });

        req.on('timeout', function() {
            console.log("timeout! " + (options.timeout / 1000) + " seconds expired");
            // Source: https://github.com/nodejs/node/blob/master/test/parallel/test-http-client-timeout-option.js#L27
            req.destroy();
        });
        //res.send(resp);
    }).end();
});

var createTaxi = function(id) {
    var t = {};
    t.id = id;
    t.x = 2.34758 - 0.05 + Math.random() / 10;
    t.y = 48.85822 - 0.05 + Math.random() / 10;
    return t;
}

app.get('/reset', function(req, res) {
    var max = req.query.max || 10000; // ex : /reset?max=100
    var response = '';

    client.del(rk(key, 'geo'), function(error, result) {
        if (error) {
            response += 'Error : ' + error
        } else {
            response += "Key " + key + " deleted. "
        }
    });

    for (var i = 1; i <= max; i++) {
        var newTaxi = createTaxi(i); // On crée un taxi avec des valeurs aléatoires

        client.geoadd(rk(key, 'geo'), newTaxi.x, newTaxi.y, newTaxi.id); // On l'ajoute au set :geo

    }
    response += 'Done adding ' + max + ' taxis.';
    res.send(response);
});


app.get('/getLive', function(req, res) {
    var response = '';

    var options = {
        host: 'intranet.sngt.g7.fr',
        path: '/pipi/api/taxis.php',
    };
    var req = http.request(options, function(resp) {
        let data = '';

        // A chunk of data has been recieved.
        resp.on('data', (chunk) => {
            data += chunk;
        });

        // The whole response has been received. Process it.
        resp.on('end', () => {

            // First we cleanup the db
            client.del(rk(key, 'geo'), function(error, result) {
                if (error) {
                    response += 'Error : ' + error
                } else {
                    response += "Key " + key + " deleted. "
                }
            });
            taxis = JSON.parse(data);
            console.log(taxis)
                // Then we add the newest positions
            for (var i = 0; i < taxis.length; i++) {
                client.geoadd(rk(key, 'geo'), taxis[i].longitude, taxis[i].latitude, taxis[i].codeTaxi); // On l'ajoute au set :geo
            }
            response += 'Récupération de ' + taxis.length + ' taxis.<br/>Retour vers la <a href = "./heatmap.html">heatmap<a/>.';
            res.send(response);
        });
    }).end();
});

var listener = app.listen(app.get('port'), '0.0.0.0', function() {
    console.log('Server listening on port: ', app.get('port'));
    console.log(listener.address());
});